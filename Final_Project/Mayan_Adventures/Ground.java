import greenfoot.*;

/**
 * Write a description of class Ground here.
 * 
 * @author Eduardo D Alarcon 
 * @version CSCI 145 Final Project Game
 */
public class Ground extends World
{
    private GreenfootSound bgMusic;
    /**
     * Constructor for objects of class Ground.
     * 
     */
    public Ground()
    {    
        super(800, 900, 1); 
        bgMusic = new GreenfootSound("sounds/Payaso_del_Rodeo.mp3");
        prepare();
    } // end constructor Ground
    
    /**
     * When Spacebar is pressed music will pause
     * Objects are renewed at given rates
     */
    public void act()
    {
        if (Greenfoot.isKeyDown("Space"))
        {
            if (bgMusic.isPlaying())
            {
                bgMusic.pause();
                Greenfoot.stop();
            } 
            else
            {
                bgMusic.play();
            } // end inner-if-statement
        } // end outer-if-statement
        
        if ( Greenfoot.getRandomNumber(100) < 1 )
        {
            //Renews Left Wall
            addObject( new Boulder(), Greenfoot.getRandomNumber(500), Greenfoot.getRandomNumber(10));
        } // end if-statement
                
        if ( Greenfoot.getRandomNumber(100) < 1 )
        {
            addObject( new Opponent(), Greenfoot.getRandomNumber(620), Greenfoot.getRandomNumber(17));
        } // end if-statement
        
        if ( Greenfoot.getRandomNumber(100) < 5 )
        {
            //Renews Left Wall
            addObject( new Walls(), 74, Greenfoot.getRandomNumber(360));
        } // end if-statement
        
        if ( Greenfoot.getRandomNumber(100) < 5 )
        {
            //Renews Right Wall
            addObject( new Walls(), 735, Greenfoot.getRandomNumber(360));
        } // end if-statement
    } // end act method 
    
    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        //Right Walls
        Walls walls = new Walls();
        addObject(walls, 735, 290);
        Walls walls2 = new Walls();
        addObject(walls2, 738, 860);
        walls2.setLocation(735, 864);

        //Left Walls
        Walls walls3 = new Walls();
        addObject(walls3, 74, 294);
        Walls walls4 = new Walls();
        addObject(walls4, 79, 865);
        walls4.setLocation(74, 866);

        //Mayan's Position
        Mayan mayan = new Mayan();
        addObject(mayan, 375, 871);
        mayan.setLocation(416, 866);
        mayan.setLocation(408, 592);
    } // end prepare method
} // end class Ground
