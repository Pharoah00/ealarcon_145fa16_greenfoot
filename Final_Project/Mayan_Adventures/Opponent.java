import greenfoot.*;

/**
 * Write a description of class Opponent here.
 * 
 * TODO: Give meaning to 
 * 
 * @author Eduardo D Alarcon 
 * @version CSCI 145 Final Project Game
 */
public class Opponent extends Actor
{
    /**
     * Act - do whatever the Opponent wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        //Makes Opponent Move Downward
        setLocation(getX(), getY() + 3);
        
        if (getY() == 899) 
        {
            //Object is removed once it reaches the end of the world
            getWorld().removeObject(this);
        } // end if-statement
    } // end act method    
} // end class Opponent
