import greenfoot.*;

/**
 * Write a description of class Walls here.
 * 
 * TODO: Set Walls to be removed once they reach the bottom of the World
 * 
 * @author Eduardo D Alarcon 
 * @version CSCI Final Project Game
 */
public class Walls extends Actor
{
    /**
     * Act - do whatever the Walls wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        //Makes Walls move downward
        setLocation(getX(), getY() + 3);
        
        if (getY() == 899) 
        {
            //Object is removed once it reaches the end of the world            
            getWorld().removeObject(this);
        } // end if-statement
    } // end act method    
} // end class Walls