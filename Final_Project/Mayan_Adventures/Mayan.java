import greenfoot.*;

/**
 * Write a description of class Mayan here.
 * 
 * TODO: 
 * Boulder jumping
 * Make Walls the limit
 * 
 * @author Eduardo D Alarcon 
 * @version CSCI 145 Final Project
 */
public class Mayan extends Actor
{
    private GreenfootImage[] images = new GreenfootImage[5];
    private int frameCount;
    private int imageIndex;
    private boolean isFacingRight;
    
    /**
     * Constructor Class
     * Images are set up here
     */
    public Mayan()
    {
        images[0] = new GreenfootImage("Link_StandTest.png");
        images[1] = new GreenfootImage("Link_Right.png");
        images[2] = new GreenfootImage("Link_Jump.png");
        images[3] = new GreenfootImage("Link_Land1.png");
        images[4] = new GreenfootImage("Link_RF.png");
        setImage(images[0]);
        
        isFacingRight = true;
    } // end constructor Mayan
    
    /**
     * Act - do whatever the Mayan wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     * 
     * Animation is created here
     */
    public void act() 
    {
        checkKeyPress();
        checkCollision();
        frameCount++;

        //Animates Object
        if (frameCount == 2)
        {
            updateImage();
            frameCount = 0;
        } // end if-statement
        //Checks if Image is Facing Right
        if (Greenfoot.isKeyDown("Left") && isFacingRight)
        {
            isFacingRight = false;
            flipImages();
        } // end if-statement
        //Checks if Image is Facing Left
        if (Greenfoot.isKeyDown("Right") && !isFacingRight)
        {
            isFacingRight = true;  
            flipImages();
        } // end if-statement
    } // end act method
        
    /**
     * For loop to Flip the Image Horizontally
     */
    public void flipImages()
    {
        for (GreenfootImage image : images)
        {
            image.mirrorHorizontally();
        } // end for-loop
    } // end flipImages method
    
    /**
     * Method to Update the Current Image
     */
    public void updateImage()
    {
        setImage(images[imageIndex]);
        if (++imageIndex > (images.length - 4))
        {
            imageIndex = 0;
        } // end if-statement
    } // end method switchImage

    /**
     * Scans for Keyboard input
     * If Right key is pressed, move 10px right
     * If Left key is pressed, move 10px left
     * If Up key is pressed, object will "Jump" over
     * Boulder and Opponent objects
     */
    public void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("Right")) 
        {
            move(10);            
        } // end if-statment
                
        if (Greenfoot.isKeyDown("Left")) 
        {
            move(-10);
        } // end if-statement
                
        if (Greenfoot.isKeyDown("Up")) 
        {
            //switchImage statements go here
        } // end if-statement
    } // end method checkKeyPress
    
    /**
     * Checks if Mayan object collides with any other object
     */
    public void checkCollision()
    {
        if (isTouching(Opponent.class)) 
        {
            Greenfoot.playSound("mario_ohno.mp3");
            Greenfoot.stop();
        } // end if-statement

        if (isTouching(Boulder.class)) 
        {
            Greenfoot.playSound("mario_ohno.mp3");
            Greenfoot.stop();
        } // end if-statement
    } // end method checkCollision
} // end class Mayan
