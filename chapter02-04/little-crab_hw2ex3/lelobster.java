/**
 *@author EALARCON@email.uscb.edu
 *@version CSCI 145 Homework 2 Exercise 1
 */
import greenfoot.*;

/**
 * Write a description of class lelobster here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class lelobster extends Actor
{
    /**
     * Act - do whatever the lelobster wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        /**
         * Do whatever lobsters do.
         */

        turnAtEdge();
        randomTurn();
        move(5);
        lookForCrab();
    }//end act method

    /**
     * Check whether we are at the edge of the world. If we are, turn a bit.
     * If not, do nothing.
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() ) 
        {
            turn(17);
        }//end if-statement
    }//end turnAtEdge method

    /**
     * Randomly decide to turn from the current direction, or not. If we turn
     * turn a bit left or right by a random degree.
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) > 90) 
        {
            turn(Greenfoot.getRandomNumber(90)-45);
        }//end if-statement
    }//end randomTurn method

    /**
     * Try to pinch a crab. That is: check whether we have stumbled upon a crab.
     * If we have, remove the crab from the game, and stop the program running.
     */
    public void lookForCrab()
    {
        if ( isTouching(Crab.class) ) 
        {
            removeTouching(Crab.class);
            Greenfoot.playSound("au.wav");
            Greenfoot.stop();
        }//end if-statment
    }//end lookForCrab method
}//end leLobster class    

