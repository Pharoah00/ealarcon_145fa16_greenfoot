/**
 * @author EALARCON@email.uscb.edu 
 * @version CSCI 145 Homework 2 Exercise 3
 */
import greenfoot.*;

/**
 * This class defines a crab. Crabs live on the beach.
 */
public class Crab extends Actor
{
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int wormsEaten;
    private int frameCount;
    //Corrected by Tutor Jeremy Suarez
    //private int currentFlipAngle = 0;
    private int currentFlipAngle;
    /**
     * Create a crab and initialize its two images.
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");
        setImage(image1);
        wormsEaten = 0;
        //Initialized correctly by Jeremy Suarez
        //currentFlipAngle initialized to zero:
        currentFlipAngle = 0;        
    }//end Crab constructor

    /**     
     * Commented out action below so Crab would only move when
     * "up" arrow key is pressed.        
     */ 
    public void act()
    {
        checkKeypress();
        //move(5); //Made the crab move.   
        lookForWorm();
        switchImage();
        checkFlipAround();
    } //end method act

    /**
     * Alternate the crab's image between image1 and image2.
     */
    public void switchImage()
    {
        frameCount = frameCount + 1;//increment frameCount by 1
        if (frameCount == 2)
        {
            //after animation, re-initialiaze frameCount to zero
            frameCount = 0;
            if (getImage() == image1) 
            {
                setImage(image2);
            }//end inner-if-statement
            else
            {
                setImage(image1);
            }//end outer-else-statement
        }//end outer-if-statement
    }//end method switchImage

    /**
     *Exercise 3.18 Method checkKeypress which commands
     *the crab to turn either left or right when left/right
     *keys are pressed.
     *
     *Homework 2 Exercise 2 Method checkKeypress commands
     *the crab to move 5 pixels forward when "up" arrow
     *key is pressed
     */
    public void checkKeypress()
    {
        //When Up key is pressed Crab will move 5 pixels forward
        if (Greenfoot.isKeyDown( "up" ) )
        {
            move( 5 );
        }//end if-statement
        //When Left Key is pressed Crab will turn Left 8 degrees
        if (Greenfoot.isKeyDown( "left" ) )
        {
            turn( -8 );
        }//end if-statement
        //When Right Key is pressed Crab will turn Right 8 degrees
        if (Greenfoot.isKeyDown( "right" ) )
        {
            turn( 8 );
        }//end if-statement 
        /**
         *I tried to have the nested if-statement placed here
         *in hopes that I would still get credit for having a
         *nested if-statement and making the crab stop when 
         *the Crab did a 180 degree turn. The problem was it
         *was wrong and I could not get the Crab object to stop.
         */
       
        //When Down Key is pressed Crab will turn Down 45 degrees
        /*
         *if (Greenfoot.isKeyDown( "down" ) )
         *{
         *   turn( 45 );
         *   if (currentFlipAngle == 45)
         *   {
         *       Greenfoot.stop();
         *   }//end inner if-statement
         *}//end if-statement 
         */
    }//end method checkKeyPress

    /**
     * Aided by Kasey Coluccio and Jeremy Suarez 
     * 
     * Method checkFlipAround created in order 
     * to make Crab "Flip Around" and stop at 
     * 180 degrees.
     * 
     * My problem with this exercise was that
     * I could not get the method to check if
     * the key was being pressed, and when it
     * did check if the "down" key was being
     * pressed it would not stop at 180 when
     * the key was held down.
     */
    public void checkFlipAround()
    {
        //if-statement to check if "down" key is pressed
        if ( Greenfoot.isKeyDown( "down" ))
        {
            /*
             * turn(45); 
             *switched turn(45) and incremented currentFlipAngle
             */
            //currentFlipAngle incremented by the same turn value 
            currentFlipAngle = currentFlipAngle + 45;
            if ( currentFlipAngle < 180 )
            {
               //switch incremented currentFlipAngle and turn(45)
               
               //currentFlipAngle = currentFlipAngle + 45;
               
               turn(45);
            }//end inner if-statement
        }//end outer if-statement
        else
        {
            //currentFlipAngle re-initialed to zero
            currentFlipAngle = 0;
        }//end else-statement
    }//end method checkFlipAround

    public void lookForWorm()
    {
        if ( isTouching (worm.class) ) //crab eat worm
        {
            removeTouching(worm.class);
            Greenfoot.playSound("slurp.wav");
            wormsEaten = wormsEaten + 1;

            if(wormsEaten == 8)
            {
                Greenfoot.playSound("fanfare.wav");
                Greenfoot.stop();
            }//end inner-if-statement
        } // end outer-is-statement
    } //end lookForWorm method

    public void lookForLobster()
    {
        if ( isTouching (lelobster.class) ) //lobster eat crab
        {
            removeTouching(lelobster.class);
        }//end if-statement
    } //end lookForLobster
} //end class Crab