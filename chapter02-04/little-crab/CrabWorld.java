import greenfoot.*;  // (Actor, World, Greenfoot, GreenfootImage)

public class CrabWorld extends World
{
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        super(560, 560, 1);//initiates 560x560 pixels
        /* 
         * instantiates a Crab object and assigns a reference to the object
         * to the myCrab variable, which is of type Crab
         */
        Crab myCrab = new Crab();
        /* 
         * Crab myCrab; //declares a variable called myCrab of type Crab
         * myCrab = new Crab();//assignment to the variable myCrab 
         */ 
        //add the newly instantiated Crab object to the scenario
        addObject( myCrab, 250, 200 );
        /*
        //Exercise 4.11
        lelobster lobster1 = new lelobster();
        lelobster lobster2 = new lelobster();
        lelobster lobster3 = new lelobster();
        addObject( lelobster(), 387, 4 );
        addObject( lelobster(), 417, 222 );
        addObject( lelobster(), 100, 216 );

        //Exercise 4.12
        worm worm1 = new worm();
        worm worm2 = new worm();
        addObject( worm1, 420, 420 );
        addObject( worm2, 301, 82 );
         */

        prepare();
    }//end CrabWorld

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        worm worm = new worm();
        addObject(worm, 103, 80);
        worm worm2 = new worm();
        addObject(worm2, 520, 132);
        worm worm3 = new worm();
        addObject(worm3, 437, 182);
        worm worm4 = new worm();
        addObject(worm4, 396, 379);
        worm worm5 = new worm();
        addObject(worm5, 176, 412);
        worm worm6 = new worm();
        addObject(worm6, 23, 310);
        worm worm7 = new worm();
        addObject(worm7, 69, 240);
        worm worm8 = new worm();
        addObject(worm8, 81, 267);
        worm worm9 = new worm();
        addObject(worm9, 129, 366);
        worm worm10 = new worm();
        addObject(worm10, 90, 447);
        lelobster lelobster = new lelobster();
        addObject(lelobster, 352, 97);
        lelobster lelobster2 = new lelobster();
        addObject(lelobster2, 465, 285);
        lelobster lelobster3 = new lelobster();
        addObject(lelobster3, 196, 477);
    }
}//end CrabWorld class