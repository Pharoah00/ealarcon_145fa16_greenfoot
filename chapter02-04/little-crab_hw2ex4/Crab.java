/**
 *@author EALARCON@email.uscb.edu
 *@version CSCI 145 Homework 2 Exercise 1
 */
import greenfoot.*;

/**
 * This class defines a crab. Crabs live on the beach.
 */
public class Crab extends Actor
{
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int wormsEaten;
    private int frameCount;
    private int currentFlipAngle;
    /**
     * Create a crab and initialize its two images.
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");
        setImage(image1);
        wormsEaten = 0;
        
    }//end Crab constructor

    public void act()
    {
        checkKeypress();
        //move(5); //Made the crab move.   
        lookForWorm();
        //Commented out switchImage as instructed
        //switchImage();
        checkFlipAround();
    } //end method act

    /**
     * Alternate the crab's image between image1 and image2.
     */
    /* Commented out switchImage method as instructed
     * 
     * public void switchImage()
     * {
     *       frameCount = frameCount + 1;//increment frameCount by 1
     *       if (frameCount == 2)
     *       {
     *           //after animation, re-initialiaze frameCount to zero
     *           frameCount = 0;
     *           if (getImage() == image1) 
     *           {
     *               setImage(image2);
     *           }//end inner-if-statement
     *           else
     *           {
     *               setImage(image1);
     *           }//end outer-else-statement
     *       }//end outer-if-statement
     * }//end method switchImage
     */   
    /**
     *Exercise 3.18 Method checkKeypress which commands
     *the crab to turn either left, right,or up when 
     *the left, right, and up keys are pressed.
     *
     *Homework 2 Exercise 4 Crab animates when only
     *arrow keys are pressed.
     */
    public void checkKeypress()
    {
        //When Up Key is pressed Crab will move Forward 5 pixels
        if (Greenfoot.isKeyDown( "up" ) )
        {
            move( 5 );
            frameCount = frameCount + 1;//increment frameCount by 1
            if (frameCount == 2)
            {
                //after animation, re-initialiaze frameCount to zero
                frameCount = 0;
                if (getImage() == image1) 
                {
                    setImage(image2);
                }//end inner-if-statement
                else
                {
                    setImage(image1);
                }//end inner-else-statement
            }//end outer-if-statement

        }//end if-statement
        
        //When Left Key is pressed Crab will turn Left 8 degrees
        if (Greenfoot.isKeyDown( "left" ) )
        {
            turn( -8 );
            frameCount = frameCount + 1;//increment frameCount by 1
            if (frameCount == 2)
            {
                //after animation, re-initialiaze frameCount to zero
                frameCount = 0;
                if (getImage() == image1) 
                {
                    setImage(image2);
                }//end inner-if-statement
                else
                {
                    setImage(image1);
                }//end inner-else-statement
            }//end outer-if-statement

        }//end if-statement

        //When Right Key is pressed Crab will turn Right 8 degrees
        if (Greenfoot.isKeyDown( "right" ) )
        {
            turn( 8 );
            frameCount = frameCount + 1;//increment frameCount by 1
            if (frameCount == 2)
            {
                //after animation, re-initialiaze frameCount to zero
                frameCount = 0;
                if (getImage() == image1) 
                {
                    setImage(image2);
                }//end inner-if-statement
                else
                {
                    setImage(image1);
                }//end outer-else-statement
            }//end outer-if-statement

        }//end if-statement

    }//end method checkKeyPress
    
    
    /**
     *Homework 2 Exercise 4 Crab animates only
     *when the down arrow key is pressed.
     */
    public void checkFlipAround()
    {
        //if-statement to check if "down" key is pressed
        if ( Greenfoot.isKeyDown( "down" ))
        {
            //currentFlipAngle incremented by the same turn value
            currentFlipAngle = currentFlipAngle + 45;
            
            //increment frameCount by 1
            frameCount = frameCount + 1;
            
            if (frameCount == 2)
            {
                //after animation, re-initialiaze frameCount to zero
                frameCount = 0;
                if (getImage() == image1) 
                {
                    setImage(image2);
                }//end inner-if-statement
                else
                {
                    setImage(image1);
                }//end outer-else-statement
            }//end outer-if-statement
            
            if ( currentFlipAngle < 180 )
            {
               turn(45);
            }//end inner if-statement
        
        }//end outer if-statement
        
        else
        {
            //currentFlipAngle re-initialized to zero
            currentFlipAngle = 0;
        }//end else-statement
    
    }//end method checkFlipAround

    public void lookForWorm()
    {
        if ( isTouching (worm.class) ) //crab eat worm
        {
            removeTouching(worm.class);
            Greenfoot.playSound("slurp.wav");
            wormsEaten = wormsEaten + 1;

            if(wormsEaten == 8)
            {
                Greenfoot.playSound("fanfare.wav");
                Greenfoot.stop();
            }//end inner-if-statement
        } // end outer-is-statement
    } //end method lookForWorm

    public void lookForLobster()
    {
        if ( isTouching (lelobster.class) ) //lobster eat crab
        {
            removeTouching(lelobster.class);
        } // end if
    } //end lookForLobster
} //end class Crab

