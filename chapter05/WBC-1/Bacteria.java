import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Bacteria fload along in the bloodstream. They are bad. Best to destroy
 * them if you can.
 * 
 * @author Eduardo Alarcon
 * @version CSCI 145 Chapter 5 Extra Credit
 */
public class Bacteria extends Actor
{
    /**
     * Constructor. Nothing to do so far.
     */
    public Bacteria()
    {
        
    } // end constructor Bacteria

    /**
     * Float along the bloodstream, slowly rotating.
     */
    public void act() 
    {
        setLocation(getX()-2, getY());
        turn(1);
        
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        } // end if-statement
    } // end act method
} // end class Bacteria
