import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Lining are objects at the edge of the vein.
 * 
 * @author Eduardo Alarcon
 * @version CSCI 145 Chapter 5 Extra Credit
 */
public class Lining extends Actor
{
    /**
      * Act
      */
    public void act() 
    {       
        /*
         * Added the first line in the Bacteria class' act method,
         * added the if-statement and changed the movememt to 1 pixel.
         */
        
        setLocation(getX()-2, getY());
        move(-1); //made the Lining move left by one pixel    
        
        // Removes the Lining objects once they reach the end of the world
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        } // end if-statement
    } // end method act    
} // end class Lining
