import greenfoot.*;

/**
 * Write a description of class RedCell here.
 * 
 * @author Eduardo Alarcon 
 * @version CSCI 145 Chapter 5 Extra Credit
 */
public class RedCell extends Actor
{
    public RedCell()
    {
        //Rotation set to rotate slowly at a random rate
        setRotation( Greenfoot.getRandomNumber(360)); 
    } // end RedCell constructor
    
    /**
     * Act - do whatever the RedCell wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        //Set the RedCell objects speed range 1 to 2
        setLocation(getX()-(Greenfoot.getRandomNumber(2) + 1), getY());
        turn(1);
        
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        } // end if-statement
    } // end act method    
} // end RedCell class
