import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is a white blood cell. This kind of cell has the job to catch 
 * bacteria and remove them from the blood.
 * 
 * @author Eduardo Alarcon
 * @version CSCI 145 Chapter 5 Extra Credit
 */
public class WhiteCell extends Actor
{
    /**
     * Act: move up and down when cursor keys are pressed.
     */
    public void act() 
    {
        checkKeyPress();
        checkCollision();
    } // end act method
    
    /**
     * Check whether a keyboard key has been pressed and react if it has.
     */
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("up")) 
        {
            setLocation(getX(), getY()-4);
        } // end if-statement
        
        if (Greenfoot.isKeyDown("down")) 
        {
            setLocation(getX(), getY()+4);
        } // end if-statement
    } // end checkKeyPress method
    
    /**
     * 
     */
    private void checkCollision()
    {
        //Added Bacteria isTouching if-statement
        if (isTouching(Bacteria.class))
        {
            //Added sound file from: soundbible.com/402-Eating-Chips.html
            Greenfoot.playSound("Eat.wav");
            removeTouching(Bacteria.class);
        } // end if-statement
        //Added Virus isTouching if-statement
        if (isTouching(Virus.class))
        {
            Greenfoot.playSound("game-over.wav");
            Greenfoot.stop();
        } // end if-statement
    } // end checkCollision method
} // end WhiteCell class
