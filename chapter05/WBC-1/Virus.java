import greenfoot.*;

/**
 * Write a description of class Virus here.
 * 
 * @author Eduardo Alarcon 
 * @version CSCI 145 Chapter 5 Extra Credit
 */
public class Virus extends Actor
{
    /**
     * Made the Virus class move left and rotate
     * Copied and pasted lines 25-31 from the Bacteria class
     */
    public void act() 
    {
        //Set objects to move four cells per act cycles
        setLocation(getX()-4, getY());
        //Set objects to rotate counter-clockwise
        turn(-1);
        
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        } // end if-statement
    } // end act method    
} // end Virus class
