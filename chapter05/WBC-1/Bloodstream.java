import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The bloodstream is the setting for our White Blood Cell scenario. 
 * It's a place where blood cells, bacteria and viruses float around.
 * 
 * @author Eduardo Alarcon
 * @version CSCI 145 Chapter 5 Extra Credit
 */
public class Bloodstream extends World
{
    /**
     * Constructor: Set up the staring objects.
     */
    public Bloodstream()
    {    
        super(780, 360, 1); 
        setPaintOrder(Border.class);
        prepare();
    } // end Bloodstream constructor

    /**
     * Create new floating objects at irregular intervals.
     */
    public void act()
    {
        //Bacteria Renewal if-statement
        if (Greenfoot.getRandomNumber(100) < 3)
        {
            addObject(new Bacteria(), 779, Greenfoot.getRandomNumber(360));
        } // end if-statement
        //Lining Renewal if-statement
        if (Greenfoot.getRandomNumber(100) < 3)
        {
            //Renews the top Linings
            addObject(new Lining(), 740, 3);
            //Renews the bottom Linings
            addObject(new Lining(), 740, 359);
        } // end if-statement
        //Virus Renewal if-statement
        if (Greenfoot.getRandomNumber(100) < 1)
        {
            //Virus Objects added at one in hundred act cycles
            addObject(new Virus(), 700, Greenfoot.getRandomNumber(360));
        } // end if-statement
        //RedCell Renewal if-statment
        if (Greenfoot.getRandomNumber(100) < 6)
        {
            //RedCell objects added at 6 in hundred act cycles
            addObject(new RedCell(), 700, Greenfoot.getRandomNumber(360));
        } // end if-statement        
    } // end act method
    
    /**
     * Prepare the world for the start of the program. In this case: Create
     * a white blood cell and the lining at the edge of the blood stream.
     */
    private void prepare()
    {
        //Added Borders from p.91 in Greentfoot
        Border border = new Border();
        addObject(border, 0, 180);
        Border border2 = new Border();
        addObject(border2, 770, 180);
        
        WhiteCell whitecell = new WhiteCell();
        addObject(whitecell, 163, 179);
        Lining lining = new Lining();
        addObject(lining, 126, 1);
        Lining lining2 = new Lining();
        addObject(lining2, 342, 5);
        Lining lining3 = new Lining();
        addObject(lining3, 589, 2);
        Lining lining4 = new Lining();
        addObject(lining4, 695, 5);
        Lining lining5 = new Lining();
        addObject(lining5, 114, 359);
        Lining lining6 = new Lining();
        Lining lining7 = new Lining();
        addObject(lining7, 295, 353);
        Lining lining8 = new Lining();
        Lining lining9 = new Lining();
        Lining lining10 = new Lining();
        addObject(lining10, 480, 358);
        Lining lining11 = new Lining();
        addObject(lining11, 596, 359);
        Lining lining12 = new Lining();
        addObject(lining12, 740, 354);
    } // end prepare method
} // end Bloodstream class
