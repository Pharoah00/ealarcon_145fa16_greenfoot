/* Ch. 3 MPL Exercises */

/* 20734 (scroll down to section 3.5) */
public class Averager 
{
	/* FIELDS */
	private int sum=0;
	private int count=0;
	
	/* CONSTRUCTORS */
	// no explicit constructors, so Java will 
	// create a default no-arg constructor for us
	
	/* METHODS */
	public int getSum()
	{
		return sum;
	} // end method getSum
	
	public void add(int a)
	{
		sum += a; // means the same thing as sum = sum + a;
		count++;  // means the same thing as count = count + 1;
	} // end method add
	
	public int getCount()
	{
		return count;
	} // end method getCount
	
	public double getAverage()
	{
		/* fill in the missing return statement
           that gives us a double result (even though
		   both sum and count are integers!) */

		return (double)sum/count;

	} // end method getAverage
	
} // end class Averager

/* 21137 */
dataTransmitter.sendSignal();

/* 20583 */
officeAC = new AirConditioner();
officeAC.turnOn();

/* 21141 */


/* 20580 */
myAc.setTemp(72);

/* 20626 */
public class Something
{

}

/* 20732 */
public class Accumulator 
{
	private int sum = 0;
	
	public int getSum()
	{
		return sum;
	} // end method getSum		
	
	public void add( int value )
	{
		// sum = sum + value;
		sum += value;
	} // end method value	
	
	
} // end class Accumulator

/* 20725 */
public class Counter 
{
	private int counter = 0;
	
	public void increment()
	{
		counter ++; // postfix increment operator
		// equivalently, in this context, we could say:
		// ++counter;
		// prefix increment operator
		
	}// end method increment
	
	public int getValue()
	{
		return counter;
	}// end method getValue
	
} // end class Counter

/* 20733 */
// Use the solution for 20732 as a starting point

public class Accumulator 
{
	private int sum = 0;
	
	public Accumulator ( int initialSum )
	{
		sum = initialSum; // could also say this.sum = intialSum;
	} // end constructor Accumulator 
	
	public int getSum()
	{
		return sum;
	} // end method getSum		
	
	public void add( int value )
	{
		// sum = sum + value;
		sum += value;
	} // end method value	

} // end class Accumulator

/* 20727 */
// Use the solution to 20725 as a starting point
public class Counter 
{
	private int counter = 0;
	
	private int  limit;
	
	public Counter( int arg1, int arg2 )
	{
		counter = arg1;
		limit = arg2;
	} // end constructor Counter
	
	public void increment()
	{
		if( counter < limit )
		{	
			counter ++;
		} // end if-statement
		
	} // end method increment
	
	public void decrement()
	{
		if( counter > 0 )
		{
			counter--;
		} // end if-statement
	} // end method decrement
	public int getValue()
	{
		return counter;
	} // end method getValue
	
} // end class Counte