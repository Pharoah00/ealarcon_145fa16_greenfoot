// Fig. 3.5: Account.java
// Account class with a constructor that initializes the name.

/**
 *
 *
 *
 * @author EALARCON@email.uscb.edu
 * @version 10/31/2016
 */
public class Account
{
   private String name; // instance variable
   private int accountNumber; // another instance variable


   /* CONSTRUCTORS */
   /*
    * "one-arg" constructor initializes
    * the instance variable name with the value
    * constructor initializes name with (parameter name)
    * NEVER FORGET THIS: As soon as you explicitly define
    * a constructor for a given class, Java no longer
    * creates a default (no-arg) constructor for you!
    */
   public Account(String name) // constructor name is class name 
   {                                                               
     /*
      * we are using the constructor to set the 
      * value of the instance variable, so we use
      * the same statement as we found in the setName
      * method (below)
      */
      this.name = name;

     /*
      * since this constructor only has one argument,
      * then by not explicitly initializing the value of
      * accountNumber, it takes the default initial value
      * of 0
      */

   } // end Account 1-arg constructor

   // overloaded constructor that takes two arguments
   public Account( String name, int accountNumber )
   {
      this.name = name;
      this.accountNumber = accountNumber;
   } // end Account 2-arg constructor

   /* METHODS */
   // method to set the name
   // mutator
   public void setName(String name)
   {
      this.name = name; 
   } // end method setName

   // method to retrieve the name
   // accessor
   public String getName()
   {
      return name; 
   } // end method getName

   public void setAccountNumber( int accountNumber)
   {
      // We use the "this" keyword to refer to
      // the current Account object, and then use
      // the dot notation here disambiguates the
      // instance variable accountNumber from the
      // local variable, which also has the name 
      // accountNumber
      this.accountNumber = accountNumber;

   } // end method setAccountNumber

   // the return type for our get method needs
   // to match the data type for the instance variable
   // that we are "getting"
   public int getaccountNumber()
   {
      // because we have specfied a non-void return
      // tyoe in the method header, we need to
      // return a value whose data type mathches
      // the return type of the method
      return accountNumber;

   } // end method getaccountNumber

} // end class Account

/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
