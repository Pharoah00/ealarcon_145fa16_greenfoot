
// Fig. 3.1: Account.java
// Account class that contains an name instance variable 
// and methods to set and get its value.

public class Account
{
   /* FIELDS */
   private String name; // instance variable, which has class-wide scope

   /* CONSTRUCTORS */
   // Note: This class does not have an explicitly
   // defined constructor, so the compiler will create
   // a default "no-argument" constructor for you.

   /* METHODS */
   // method to set the name in the object              
   // this would be an example of a "setter" or "mutator" method
   public void setName(String name)      
   {                                             
      // we have a local variable called name,
      // defined in the parameter list above
      // (this local variable, name, has method-wide scope)
      // the keyboard "this" refers to the current Account object
      this.name = name; // store the name
   } // end method setName         

   // method to retrieve the name from the object
   // getName has non-void return type (String),
   // so you could think of this method as asking a question
   // of the Account object ("What is your name?")
   // this would be an example of a "getter" or "accessor" method
   public String getName()        
   {                                    
      return name; // return value of name to caller        
   } // end method getName    

} // end class Account


/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
