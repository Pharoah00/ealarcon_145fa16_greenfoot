// Fig. 3.2: AccountTest.java
// Creating and manipulating an Account object.
import java.util.*;//this is an aexample of a wildcard import
// We don't explicitly import java.lang

/**
 * This is a driver or application class (or sometimes
 * simply known as a "test" class) for testing the
 * capabilities pf the Account class and any objects 
 * instantiated from the Account class
 *
 * @author EALARCON@email.uscb.edu
 * @version 10/28/2016
 */

public class AccountTest
{
   public static void main(String[] args)
   { 
      // create a Scanner object to obtain input from the command window
      Scanner input = new Scanner(System.in);

      // create an Account object and assign it to myAccount
      Account myAccount = new Account(); 

      // display initial value of name (null)
      // %s is a format specifier
      System.out.printf("Initial name is: %s\n\n", myAccount.getName());

      // prompt for and read name
      System.out.println("Please enter the name:");
      String theName = input.nextLine(); // read a line of text
      
      // we call the setName method, which is expecting
      // a single argument of type String
      myAccount.setName(theName); // put theName in myAccount
      System.out.println(); // outputs a blank line

      // display the name stored in object myAccount
      System.out.printf("Name in object myAccount is:\n%s\n",
      myAccount.getName());
   } // end method main
} // end class AccountTest


/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
